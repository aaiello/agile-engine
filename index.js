'use strict'

var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(session({ secret: 'this-is-a-secret-token', cookie: { maxAge: 60000 }}));
 
// Access the session as req.session
app.get('/', function(req, res, next) {
  res.send('Start');
});

app.getEmptyBalance = function(){
  return {"history": [{
        "value": 10,
        "title": "Just for test",
        "description": "This transaction is just for test"
    }, {
        "value": -5,
        "title": "Just for test with negative",
        "description": "This transaction is just for test with negative value"
    }], "balance": 5};
}

app.post('/new-transaction', function(req, res) {
  var newTransaction = req.body;

  var creditHistory = app.getEmptyBalance();
  
  var sessData = req.session;
  if(sessData.creditHistory)
    creditHistory = req.session.creditHistory;

  if(newTransaction.value == 0){
    res.status(500)        // HTTP status 404: NotFound
   .send('Zero transaction');
   return;
  }

  if(newTransaction.value + creditHistory.balance<0){
    res.status(500)        // HTTP status 404: NotFound
   .send('Negative balance');
   return;
  }

  creditHistory.history.push( newTransaction );
  creditHistory.balance += newTransaction.value; 
  sessData.creditHistory  = creditHistory;

  res.send(`ok`);
});


app.get('/get-history', function(req, res, next) {
  var creditHistory = app.getEmptyBalance();
  
  var sessData = req.session;
  if(sessData.creditHistory)
    creditHistory = req.session.creditHistory.history;

  res.setHeader('access-control-allow-origin', '*');
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(creditHistory));
});

app.get('/clear', function(req, res, next) { 
  var sessData = req.session;
  sessData.creditHistory = app.getEmptyBalance();
  
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(sessData.creditHistory));
});

app.use((req, res) => {
  res.status(404).send('') //not found
})

app.listen(3000, () => {
  console.log('server started');
});


import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import "./App.css";
import TransactionItem from "./components/TransactionItem/TransactionItem";
import Logo from "./assets/logo.png";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      // this is where the data goes
      list: []
    };
  }

  handleErrors(response) {
    if (response.cod === "404") {
      this.showError(response.message);

      throw new Error(response.message);
    }
    if (response.cod === "500") {
      this.showError(response.message);

      throw new Error(response.message);
    }
    return response;
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch(
      `https://suburbanIntentionalImplementation--five-nine.repl.co/get-history`
    )
      .then(response => response.json())
      .then(this.handleErrors)
      .then(data => this.setState({ isLoading: false, list: data.history }))
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { isLoading, list } = this.state;

    if (isLoading) {
      return <p>Loading ...</p>;
    }
    return (
      <div className="App">
        <img className="Logo" src={Logo} alt="React logo" />
        <h1 className="App-Header">Agile Engine</h1>
        <div className="App-Container">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Description</TableCell>
                <TableCell numeric>Value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.list.map((item, key) => {
                return <TransactionItem key={key} item={item} />;
              })}
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

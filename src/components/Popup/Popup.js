import React, { Component } from "react";
import "./Popup.css";

export default class Popup extends React.Component {
  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <h1>{this.props.item.title}</h1>
          <p>Value: {this.props.item.value}</p>
          <p>Description: {this.props.item.description}</p>
          <button onClick={this.props.closePopup}>Close</button>
        </div>
      </div>
    );
  }
}

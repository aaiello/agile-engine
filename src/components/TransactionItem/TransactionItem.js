import React, { Component } from "react";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Popup from "../Popup/Popup";

import "./TransactionItem.css";

export default class TransactionItem extends Component {
  constructor() {
    super();
    this.state = {
      showPopup: false
    };
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  render() {
    return (
      <TableRow
        className={
          this.props.item.value > 0
            ? "TransactionItem-positive"
            : "TransactionItem-negative"
        }
        onClick={this.togglePopup.bind(this)}
      >
        {this.state.showPopup ? (
          <Popup
            item={this.props.item}
            closePopup={this.togglePopup.bind(this)}
          />
        ) : null}
        <TableCell className="TransactionItem-Text">
          {this.props.item.title}
        </TableCell>
        <TableCell className="TransactionItem-Text">
          {this.props.item.value}
        </TableCell>
      </TableRow>
    );
  }
}

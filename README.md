# Project Name

Agile Engine Test

index.js is node server. For testing I use this url: https://repl.it/repls/SuburbanIntentionalImplementation , but this shoul be change in app.js componentDidMount method.<br>

Node has two methods:<br>
1) get-history: return all history<br>
2) new-transaction: add new transaction<br>
Expect one parameter as json post in this format:

	{	
		"value": 10,
        "title": "Just for test",
        "description": "This transaction is just for test"
    }


When an error occurs return 500 error with description.<br>

React App only use get-history. Just for testing session start with two transaction.

## History

TODO: Write history

## Credits

Andres Aiello aaiello@gmail.com

## License

TODO: Write license